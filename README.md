# Observer Priority module for Magento 2

### Features

The module allows you to set the order in which observer instances are executed while dispatching events


### Usage

1. Install module via composer:
```
 composer require driiiftking/module-observers-priority
 bin/magento module:enable Driiiftking_ObserversPriority
 bin/magento setup:upgrade
```

2. In any events.xml file you can use optional `priority` attribute to set order of executing.
   Higher priority means earlier execution of the observer. Default priority is 0.

Native events.xml file:
```
    <event name="some_magento_event">
        <observer name="observer_third" instance="Vendor\Module\Observer\Third"/>
        <observer name="observer_first" instance="Vendor\Module\Observer\First"/>
        <observer name="observer_last" instance="Vendor\Module\Observer\Last"/>
        <observer name="observer_second" instance="Vendor\Module\Observer\Second"/>
    </event>
```
Likely order of execution: **Third > First > Last > Second**

Modified events.xml file:
```
    <event name="some_magento_event">
        <observer name="observer_third" instance="Vendor\Module\Observer\Third" priority="10"/>
        <observer name="observer_first" instance="Vendor\Module\Observer\First" priority="30"/>
        <observer name="observer_last" instance="Vendor\Module\Observer\Last"/>
        <observer name="observer_second" instance="Vendor\Module\Observer\Second" priority="20"/>
    </event>
```
Order of execution: **First > Second > Third > Last**


CLI debug tool:

```
bin/magento dev:observer:info event_name [--area=areacode]
```
Example
```
bin/magento dev:observer:info cms_page_render --area=frontend
```


