<?php
declare(strict_types=1);
/**
 * @copyright Copyright (c) 2022 Adam Dudel (https://gitlab.com/dudel.adam)
 */

namespace Driiiftking\ObserversPriority\Event\Config;

class SchemaLocator extends \Magento\Framework\Event\Config\SchemaLocator
{
    public const PRIORITY = 'priority';
    public const URN_SCHEMA = 'urn:magento:module:Driiiftking_ObserversPriority:etc/events.xsd';

    /**
     * @inheirtDoc
     */
    public function getSchema(): string
    {
        return $this->urnResolver->getRealPath(self::URN_SCHEMA);
    }

    /**
     * @inheirtDoc
     */
    public function getPerFileSchema(): string
    {
        return $this->urnResolver->getRealPath(self::URN_SCHEMA);
    }
}
