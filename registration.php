<?php
declare(strict_types=1);
/**
 * @copyright Copyright (c) 2022 Adam Dudel (https://gitlab.com/dudel.adam)
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Driiiftking_ObserversPriority', __DIR__);
