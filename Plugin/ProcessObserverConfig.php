<?php
declare(strict_types=1);
/**
 * @copyright Copyright (c) 2022 Adam Dudel (https://gitlab.com/dudel.adam)
 */

namespace Driiiftking\ObserversPriority\Plugin;

use Driiiftking\ObserversPriority\Event\Config\SchemaLocator;
use Magento\Framework\Event\Config\Converter;

class ProcessObserverConfig
{
    /**
     * @param Converter $subject
     * @param array $result
     * @param DOMNode $observerConfig
     * @return array
     */
    public function after_convertObserverConfig(Converter $subject, array $result, $observerConfig): array
    {
        $priority = 0;
        if ($observerPriority = $observerConfig->attributes->getNamedItem(SchemaLocator::PRIORITY)) {
            $priority = (int) $observerPriority->value;
        }

        $result[SchemaLocator::PRIORITY] = $priority;
        return $result;
    }
}
