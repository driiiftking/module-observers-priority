<?php
declare(strict_types=1);
/**
 * @copyright Copyright (c) 2022 Adam Dudel (https://gitlab.com/dudel.adam)
 */

namespace Driiiftking\ObserversPriority\Plugin;

use Driiiftking\ObserversPriority\Event\Config\SchemaLocator;
use Magento\Framework\Event\Config;

class GetSortedObservers
{
    public const INSTANCE = 'instance';

    /**
     * @param Config $subject
     * @param array $result
     * @return array
     */
    public function afterGetObservers(Config $subject, array $result): array
    {
        $parsedObservers = $this->processObserverData($result);
        usort($parsedObservers, [self::class, 'sortObservers']);
        return $parsedObservers;
    }

    /**
     * @param array $observers
     * @return array
     */
    private function processObserverData(array $observers): array
    {
        $result = [];

        foreach ($observers as $key => $observer) {
            if (!array_key_exists(self::INSTANCE, $observer)) {
                $result = array_merge($result, $this->processObserverData($observer));
                continue;
            }

            $result[] = $observer;
        }

        return $result;
    }

    /**
     * @param $observerA
     * @param $observerB
     * @return int
     */
    private static function sortObservers($observerA, $observerB): int
    {
        return (int) ($observerB[SchemaLocator::PRIORITY] - $observerA[SchemaLocator::PRIORITY]);
    }
}
