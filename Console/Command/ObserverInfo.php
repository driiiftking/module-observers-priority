<?php
declare(strict_types=1);
/**
 * @copyright Copyright (c) 2022 Adam Dudel (https://gitlab.com/dudel.adam)
 */

namespace Driiiftking\ObserversPriority\Console\Command;

use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Magento\Framework\Event\Config;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\Area;

class ObserverInfo extends Command
{
    public const EVENT_NAME = 'event';
    public const OPTION_AREA = 'area';

    /**
     * @var Config
     */
    protected Config $observersConfig;

    /**
     * @var State
     */
    private State $state;

    /**
     * @param Config $observersConfig
     * @param State $state
     * @param string|null $name
     */
    public function __construct(Config $observersConfig, State $state, ?string $name = null)
    {
        parent::__construct($name);
        $this->observersConfig = $observersConfig;
        $this->state = $state;
    }

    /**
     * Initialization of the command.
     */
    protected function configure()
    {
        $this->setName('dev:observer:info');
        $this->setDescription('Detailed info about observers');
        $this->setDefinition([
        new InputArgument(self::EVENT_NAME, InputArgument::REQUIRED, 'Event name'),
        new InputOption(
            self::OPTION_AREA,
            null,
            InputArgument::OPTIONAL,
            'Observer area (global, adminhtml, frontend etc.)',
            Area::AREA_GLOBAL)
        ]);
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $eventName = $input->getArgument(self::EVENT_NAME);
        $areaCode = $input->getOption(self::OPTION_AREA);

        $output->setDecorated(true);
        $output->writeln('');
        $output->writeln(sprintf('Observer configuration for the event %s in %s area', $eventName, $areaCode));

        try {
            $this->state->setAreaCode($areaCode);
            $this->printObservers($output, $eventName);
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            return Cli::RETURN_FAILURE;
        }

        return Cli::RETURN_SUCCESS;
    }

    /**
     * @param OutputInterface $output
     * @param string $eventName
     * @return void
     */
    private function printObservers(OutputInterface $output, string $eventName)
    {
        $observers = $this->observersConfig->getObservers($eventName);

        $parsedData = array_map(function ($observer) {
            return [
                $observer['name'],
                $observer['instance'],
                $observer['priority']
            ];
        }, $observers);

        $table = new Table($output);
        $table->setHeaders(['Name', 'Class', 'Priority'])
            ->setRows($parsedData);

        $output->writeln($table->render());
    }
}
